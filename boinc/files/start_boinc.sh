#!/bin/bash
cd /data
/usr/bin/boinc --dir /data --no_gui_rpc --no_gpus --abort_jobs_on_exit --exit_when_idle --fetch_minimal_work --attach_project ${PROJECT_URL} ${PROJECT_KEY}

