# Copyright (c) 2016, Daniele Venzano
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Spark-Jupyter Zoe application description generator."""

import json
import sys
import os

APP_NAME = 'boinc'
ZOE_APPLICATION_DESCRIPTION_VERSION = 3

options = {
    'project_url': {
        'value': 'www.worldcommunitygrid.org',
        'description': 'BOINC project URL'
    },
    'project_key': {
        'value': 'Your key here',
        'description': 'BOINC project key'
    },
    'memory_limit': {
        'value': 512 * (1024**2),
        'description': 'Memory limit (bytes)'
    },
    'core_limit': {
        'value': 1,
        'description': 'Number of cores to allocate'
    }
}

REPOSITORY = os.getenv("REPOSITORY", default="zapps")
VERSION = os.getenv("VERSION", default="latest")

IMAGE = REPOSITORY + '/boinc:' + VERSION

def boinc_service(memory_limit, core_limit, project_key, project_url):
    """
    :rtype: dict
    """
    mem_limit_mb = memory_limit / (1024**2)
    service = {
        'name': "boinc-client",
        'image': IMAGE,
        'monitor': True,
        'resources': {
            "memory": {
                "min": memory_limit,
                "max": memory_limit
            },
            "cores": {
                "min": core_limit,
                "max": core_limit
            }
        },
        'ports': [],
        'environment': [
            ['PROJECT_URL', project_url],
            ['PROJECT_KEY', project_key]
        ],
        'volumes': [],
        'command': None,
        'total_count': 1,
        'essential_count': 1,
        'startup_order': 0,
        'replicas': 1
    }

    return service


if __name__ == '__main__':
    app = {
        'name': APP_NAME,
        'version': ZOE_APPLICATION_DESCRIPTION_VERSION,
        'will_end': False,
        'size': 128,
        'services': [
            boinc_service(options["memory_limit"]["value"], options["core_limit"]["value"], options["project_key"]["value"], options["project_url"]["value"])
        ]
    }

    json.dump(app, open("boinc.json", "w"), sort_keys=True, indent=4)
    with open('images', 'w') as fp:
        fp.write(IMAGE + '\n')

    print("ZApp written")

